import math
import json
import logging
import requests
import pandas as pd
from typing import *
import kfserving


logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


class PreprocessTransformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.model_name = name
        self.predictor_url = predictor_host
        logging.info(f"MODEL NAME {self.name}")
        logging.info(f"PREDICTOR URL {self.predictor_url}")

    def preprocess(self, inputs: Dict[str, List[Dict]]) -> Dict:
        def correlate(instance):
            pdf = pd.DataFrame(instance["data"])
            pdf["raw_input_sum"] = pdf["raw_gled_value"] + pdf["raw_alice_value"]
            corr_input_enr = pdf["raw_input_sum"].corr(pdf["enr_transfer_value"])
            return [corr_input_enr if not math.isnan(corr_input_enr) else 1.0]

        logging.info(f"Preprocess-Transformation called with {len(inputs['instances'])} instances")
        preprocessed_data = {"instances": list(map(correlate, inputs["instances"]))}
        logging.info("Preprocess-Transformation successful, calling model:predict")
        return preprocessed_data

    async def predict(self, request: Dict) -> Dict:
        logging.info(request)
        req_body = json.dumps(request)

        response = requests.post(
            f"http://ml.cern.ch/v1/models/{self.model_name}:predict",
            headers={"Host": f"{self.predictor_url}.svc.cluster.local"},
            data=req_body
        )
        logging.info(response.text)
        return response.json()
